/* GPIO handler routines */

/* (C) 2016 by sysmocom s.f.m.c. GmbH <info@sysmocom.de>
 * All Rights Reserved
 *
 * Author: Philipp Maier
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#define MAX_GPIOS 16

/* Struct holding the GPIO file descriptors and
   meta information about the GPIO itsself */
struct gpio_descr {
	unsigned int gpios[MAX_GPIOS];
	unsigned int gpios_len;
	FILE *fd_direction[MAX_GPIOS];
	FILE *fd_value[MAX_GPIOS];
};

/* Open sysfs endpoints and initalize GPIOs */
struct gpio_descr *gpio_open(void *ctx, unsigned int *gpios,
			     unsigned int gpios_len);

/* Close sysfs endpoints and unintialize GPIOs */
void gpio_close(struct gpio_descr *gd);

/* Set GPIO pin status */
void gpio_set(struct gpio_descr *gd, unsigned int gpio, unsigned int state);
